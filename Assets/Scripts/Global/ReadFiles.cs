﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Master
{
    public class ReadFiles : MonoBehaviour
    {
        string settingsPath = Application.streamingAssetsPath + "/StartupSettings.txt";

        private void Awake()
        {
            if (File.Exists(settingsPath))
            {
                string[] lines = System.IO.File.ReadAllLines(settingsPath);


                if (lines[0].Split(':')[1].Trim() == "lotto")
                {
                    SceneManager.LoadSceneAsync("Lotto");
                }
                else if (lines[0].Split(':')[1].Trim() == "spinthewheel")
                {
                    SceneManager.LoadSceneAsync("SpinTheWheel");
                }
                else if (lines[0].Split(':')[1].Trim() == "keno")
                {
                    SceneManager.LoadSceneAsync("Keno");
                }
                else if (lines[0].Split(':')[1].Trim() == "videopresentor")
                {
                    SceneManager.LoadSceneAsync("VideoPresentor");
                }
                else if (lines[0].Split(':')[1].Trim() == "lucky6")
                {
                    SceneManager.LoadSceneAsync("Lucky6");
                }
                else if (lines[0].Split(':')[1].Trim() == "lucky7")
                {
                    SceneManager.LoadSceneAsync("Lucky7");
                }
                else if (lines[0].Split(':')[1].Trim() == "kenovideopresentor")
                {
                    SceneManager.LoadSceneAsync("KenoVideoPresentor");
                }
                else if (lines[0].Split(':')[1].Trim() == "super7videopresentor")
                {
                    SceneManager.LoadSceneAsync("Super7VideoPresentor");
                }
                else if (lines[0].Split(':')[1].Trim() == "bingo")
                {
                    SceneManager.LoadSceneAsync("Bingo");
                }
            }
        }
       
    }

}