﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Master
{
    public class GameController : MonoBehaviour
    {

        public GameObject avatarPos;
        public GameObject avatarScale;
        public float minX = -7.7f;
        public float maxX = 7.7f;
        public float minY = -13.8f;
        public float maxY = 5.4f;
        public float originalZ;
        public float originalScale = 5.1724f;
        private void Awake()
        {
            originalZ = transform.position.z;
        }
        public void toggleAvatar(bool status)
        {
            avatarPos.SetActive(status);
            avatarScale.SetActive(status);
        }

        public void moveAvatar(float x,float y)
        {
            avatarPos.transform.position = new Vector3(Remap(x, 0, 1920, minX, maxX), Remap(y, 0, 1080, minY, maxY), originalZ);
        }

        public void scaleAvatar(float x)
        {
            avatarScale.transform.localScale = new Vector3(x, x, x);
        }

        public float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}

