﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Master
{
    public class RemoteCommand : MonoBehaviour
    {
        public GameController con;
        public Image backgroundImage;

        private void Awake()
        {
            con = GetComponent<GameController>();
        }

        protected virtual void Action_ShowRawByteLength(byte[] _byte)
        {
            if (System.Text.Encoding.UTF8.GetString(_byte) == "ShowAvatar")
            {
                con.toggleAvatar(true);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "HideAvatar")
            {
                con.toggleAvatar(false);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "AvatarPosition")
            {
                float x = float.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim().Split(',')[0]);
                float y = float.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim().Split(',')[1]);
                con.moveAvatar(x, y);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "AvatarScale")
            {
                float x = float.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim().Split(',')[0]);
                con.scaleAvatar(x);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "SetBackground")
            {
                string bgPath = Application.streamingAssetsPath + "/" +  System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim();
                if (File.Exists(bgPath))
                {
                    byte[] pngBytes = System.IO.File.ReadAllBytes(bgPath);
                    Texture2D tex = new Texture2D(2, 2);

                    tex.LoadImage(pngBytes);
                    Sprite tempSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                    backgroundImage.sprite = tempSprite;
                }

            
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Lotto")
            {
                SceneManager.LoadSceneAsync("Lotto");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "SpinTheWheel")
            {
                SceneManager.LoadSceneAsync("SpinTheWheel");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Keno")
            {
                SceneManager.LoadSceneAsync("Keno");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "VideoPresentor")
            {
                SceneManager.LoadSceneAsync("VideoPresentor");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "KenoVideoPresentor")
            {
                SceneManager.LoadSceneAsync("KenoVideoPresentor");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Super7VideoPresentor")
            {
                SceneManager.LoadSceneAsync("Super7VideoPresentor");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Lucky6")
            {
                SceneManager.LoadSceneAsync("Lucky6");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Lucky7")
            {
                SceneManager.LoadSceneAsync("Lucky7");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Bingo")
            {
                SceneManager.LoadSceneAsync("Bingo");
            }

        }
    }
}

