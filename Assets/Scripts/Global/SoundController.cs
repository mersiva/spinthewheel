﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundController : MonoBehaviour
{
    public delegate void OnPlayAudioOnce(string c);
    public static OnPlayAudioOnce onPlayAudioOnce;

    public delegate void OnPlayAudioLoop(string c);
    public static OnPlayAudioLoop onPlayAudioLoop;

    AudioSource audioSource;

    public string url;
    public AudioSource source;

    public static SoundController _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            audioSource = GetComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }
    }


    IEnumerator loadAudio(string s,bool isLoop)
    {
        if (File.Exists(Application.streamingAssetsPath + "/Sound/" + s))
        {
            using (var www = new WWW(Application.streamingAssetsPath + "/Sound/" + s))
            {
                yield return www;
                audioSource.clip = www.GetAudioClip();

                audioSource.Play();
                if (!isLoop)
                {
                    audioSource.loop = false;
                }
                else
                {
                    audioSource.loop = true;
                }
            }
        }
        else
            yield return null;
       
    }


    private void OnEnable()
    {
        onPlayAudioOnce += playOnce;
        onPlayAudioLoop += playLoop;
        SceneManager.sceneLoaded += resetAudio;
    }

    private void OnDisable()
    {
        onPlayAudioOnce -= playOnce;
        onPlayAudioLoop -= playLoop;
        SceneManager.sceneLoaded -= resetAudio;
    }

    void resetAudio(Scene scene, LoadSceneMode mode)
    {
        audioSource.Stop();
        audioSource.clip = null;
    }

    void playOnce(string c)
    {
        StartCoroutine(loadAudio(c,false));
    }

    void playLoop(string c)
    {
        StartCoroutine(loadAudio(c,true));
    }


    private void OnDestroy()
    {
        onPlayAudioOnce = null;
        onPlayAudioLoop = null;
    }
}
