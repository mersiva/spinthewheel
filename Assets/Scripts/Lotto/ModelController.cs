﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lotto
{
    public class ModelController : MonoBehaviour
    {
        // Start is called before the first frame update
        Animator animator;

        public delegate void OnTriggerAnim(string str);
        public static OnTriggerAnim onTriggerAnim;


        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void OnDestroy()
        {
            onTriggerAnim = null;
        }

        private void OnEnable()
        {
            GameController.onPickBall += spinStarted;
            onTriggerAnim += triggerAnim;
        }

        private void OnDisable()
        {
            GameController.onPickBall -= spinStarted;
            onTriggerAnim -= triggerAnim;
        }

        void triggerAnim(string str)
        {
            animator.SetBool(str, true);
        }

        void spinStarted()
        {

            animator.SetBool("spinningStart", true);
        }

        void spinStop()
        {
            animator.SetBool("spinningStart", false);
        }
    }

}