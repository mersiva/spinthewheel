﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Lotto
{

    public class GameController : Master.GameController
    {
        public BallSet ballset;
        public List<Sprite> ballSetSequence;
        public List<Color> ballSetSequenceColors;
        public List<Sprite> numbersSprite;

        public Vector3 machineMode1Pos, machineMode2Pos;
        public Vector3 machieMode1Scale, machineMode2Scale;

        public Vector3 exitPos;
        public Vector3 restingPos;
        public Vector3 ballSpawnMode1PickedScale, ballSpawnMode2PickedScale;
        public Vector3 winningAreaScale;

        public GameObject winningArea;

        public Vector3 winningAreaScale2;
        public GameObject winningArea2;
        public GameObject prefab2;

        public delegate void OnPlayVo(int index);
        public static OnPlayVo onPlayVo;

        public delegate void OnPlayFinalNumber(int index,float delay);
        public static OnPlayFinalNumber onPlayFinalNumber;



        bool ballPicked = false;


        public GameMode currentGameMode;

        public enum GameMode
        {
            Mode1,
            Mode2
        }

        public string gameMode;

        public Vector3 getBallPickedscale()
        {
           switch(currentGameMode)
            {
                case GameMode.Mode1:
                    return ballSpawnMode1PickedScale;

                default:
                    return ballSpawnMode2PickedScale;
            }
        }


        public int totalBallBeforeColorSequenceStart;
        public int highestNumber;
        public int totalBallsToPick;
        public GameObject prefab;
        public GameObject animatedBallPrefab;
        public GameObject spawnTarget;


        public int[] balls;


        public delegate void OnNewGame();
        public static OnNewGame onNewGame;

        public delegate void OnPickBall();
        public static OnPickBall onPickBall;

        float currentTimer;
        public float pickBallDelay = 5f;

        int totalballsPicked;
   
        public GameObject machine;

        private void OnDestroy()
        {
            onNewGame = null;
            onPickBall = null;
            onPlayFinalNumber = null;
            onPlayVo = null;

        }

        public bool prefabQuaternionIdenty = true;

        private void Start()
        {
            if(machine!=null)
            {
                if (currentGameMode == GameMode.Mode1)
                {
                    machine.transform.localPosition = machineMode1Pos;
                    machine.transform.localScale = machieMode1Scale;
                }
                else if (currentGameMode == GameMode.Mode2)
                {
                    machine.transform.localPosition = machineMode2Pos;
                    machine.transform.localScale = machineMode2Scale;
                }
            }

            spawnTarget.transform.localPosition = exitPos;

            if(winningArea!=null)
                winningArea.transform.localPosition = restingPos;
            newGame();
        }

        public void newGame()
        {
            if (winningArea != null)
            {
                foreach (Transform child in winningArea.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            if (gameMode=="lucky6" )
            {
                Lucky6PositionController posCon = GameObject.Find("BallPositionController").GetComponent<Lucky6PositionController>();
                foreach (Transform child in posCon.transform)
                {
                    if (child.childCount > 0)
                    {
                        foreach (Transform subchild in child.transform)
                        {
                            Destroy(subchild.gameObject);
                        }
                    }
                      
                }
                posCon.resetPos();
            }
            else if (gameMode =="bingo")
            {

            }


            if (winningArea2 != null)
            {
                foreach (Transform child in winningArea2.transform)
                {
                    Destroy(child.gameObject);
                }
            }

             generateBalls();
            currentTimer = 0;
            totalballsPicked = 0;
            ballPicked = false;
        }

        public void Shuffle(System.Random rng, int[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                int temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }


        }


        void generateBalls()
        {
            balls = new int[highestNumber];
            int counter = 1;
            for(int i = 0; i< highestNumber;i++)
            {
                balls[i] = counter;
                counter++;
            }

            Shuffle(new System.Random(), balls);
        }

        int remapMode(int index)
        {
            if (index == 0)
                return totalBallBeforeColorSequenceStart - 1;
            else
                return index - 1;
        }

        public void pickBall(int number,Color color,Sprite sprite)
        {
            if (onPickBall != null)
                onPickBall();

            if (onPlayVo != null)
            {
                onPlayVo(number);
            }

            totalballsPicked++;

            int ballPicked = number;

            //original use spawntargetposition
            GameObject temp = null;
            if (gameMode == "bingo")
            {
               temp = Instantiate(prefab, Vector3.zero, Quaternion.identity);
                temp.transform.SetParent(spawnTarget.transform, false);
                temp.transform.localScale = Vector3.zero;
                temp.GetComponent<Ball>().text.text = ballPicked.ToString();
                temp.GetComponent<Image>().sprite = sprite;
            }
            else
            {
                temp = Instantiate(prefab, Vector3.zero, Quaternion.identity);
                temp.transform.SetParent(spawnTarget.transform, false);
                temp.GetComponent<Ball>().text.text = ballPicked.ToString();
                temp.GetComponent<Image>().sprite = sprite;
            }

    


            GameObject animatedBall = Instantiate(animatedBallPrefab, animatedBallPrefab.transform.position, animatedBallPrefab.transform.rotation);
            if(!prefabQuaternionIdenty)
            {
                animatedBall.transform.rotation = animatedBallPrefab.transform.rotation;
            }

            Material matBall;
            if (animatedBall.transform.GetChild(0).transform.childCount>0)
                matBall = animatedBall.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().material;
            else
                matBall = animatedBall.transform.GetChild(0).GetComponent<MeshRenderer>().material;

            matBall.SetColor("_Color", color);
            matBall.SetTexture("_TextureSample0", numbersSprite[ballPicked - 1].texture);
            StartCoroutine(destroyAnimatedBall(animatedBall,temp));
        }

        public void pickBall()
        {
            if (onPickBall != null)
                onPickBall();


            totalballsPicked++;

            int ballPicked = balls[0];
            balls = balls.Where((source, index) => index != 0).ToArray();

            GameObject temp = Instantiate(prefab, spawnTarget.transform.position, Quaternion.identity);
            temp.transform.SetParent(spawnTarget.transform, false);
            temp.GetComponent<Ball>().text.text = ballPicked.ToString();
            temp.GetComponent<Image>().sprite = ballSetSequence[remapMode(ballPicked % totalBallBeforeColorSequenceStart)];
      

            //Sequence mySequence = DOTween.Sequence();
            //mySequence.Join(DOTween.To(() => temp.transform.localScale, x => temp.transform.localScale = x, getBallPickedscale(), 1).SetEase(Ease.OutBounce)).OnComplete(() =>
            //{
            //    moveBallToWinningArea();
            //});

            GameObject animatedBall = Instantiate(animatedBallPrefab, animatedBallPrefab.transform.position, Quaternion.identity);
            Material matBall = animatedBall.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().material;
            matBall.SetColor("_Color", ballSetSequenceColors[remapMode(ballPicked % totalBallBeforeColorSequenceStart)]);
            matBall.SetTexture("_TextureSample0", numbersSprite[ballPicked-1].texture);
            StartCoroutine(destroyAnimatedBall(animatedBall, temp));
        }

        IEnumerator destroyAnimatedBall(GameObject animBall,GameObject ball2d)
        {
            yield return new WaitForSeconds(2);
            Destroy(animBall);

            ball2d.transform.localScale = winningAreaScale;

            if (winningArea != null)
            {
                ball2d.transform.SetParent(winningArea.transform, false);
            }
            else
            {
                Lucky6PositionController posCon = GameObject.Find("BallPositionController").GetComponent<Lucky6PositionController>();
                ball2d.transform.SetParent(posCon.getCurrentPos(),false);
                ball2d.transform.localPosition = Vector3.zero;
                posCon.incrementIndex();
            }
         

            if(winningArea2!=null)
            {
                GameObject temp = Instantiate(prefab2, Vector3.zero, Quaternion.identity);
                temp.GetComponent<Ball>().text.text = ball2d.GetComponent<Ball>().text.text;
                temp.GetComponent<Image>().sprite = ball2d.GetComponent<Image>().sprite;

                temp.transform.localScale = winningAreaScale2;
                temp.transform.SetParent(winningArea2.transform, false);
            }



            ball2d = null;
            ballPicked = true;

            yield return null;
        }


        private void Update()
        {
            //if(currentTimer < pickBallDelay)
            //{
            //    currentTimer += Time.deltaTime;
            //}
            //else
            //{
            //    currentTimer = 0;

            //    if(totalballsPicked < totalBallsToPick)
            //    {
            //        pickBall();
            //    }
            //    else if(!ballPicked)
            //    {

            //        newGame();
            //    }
            //}
        }
    }

}
