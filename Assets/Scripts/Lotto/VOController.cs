﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Lotto
{
    public class VOController : MonoBehaviour
    {
        public List<AudioClip> vo = new List<AudioClip>();
        public AudioSource audioPlayer;
        public AudioSource audioPlayer2;
        private void OnEnable()
        {
            //VideoPlaybackUDP.onPlayVo += playAudio;
            Debug.LogError("enabled");
            GameController.onPlayVo += playAudio;
            GameController.onPlayFinalNumber += playAudio2;
        }

        private void OnDisable()
        {
            //VideoPlaybackUDP.onPlayVo -= playAudio;
            Debug.LogError("disabled");
            GameController.onPlayVo -= playAudio;
            GameController.onPlayFinalNumber -= playAudio2;
        }

        void playAudio(int index)
        {
            if(index-1 <= vo.Count-1)
                audioPlayer.PlayOneShot(vo[index-1]);
        }

        void playAudio2(int index,float delay)
        {
            StartCoroutine(playDelayedAudio(index, delay));
        }

        IEnumerator playDelayedAudio(int index, float delay)
        {
            yield return new WaitForSeconds(delay);

            if (index - 1 <= vo.Count - 1)
                audioPlayer2.PlayOneShot(vo[index - 1]);

        }

    }
}
