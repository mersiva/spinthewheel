﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lotto
{
    public class RemoteCommand : Master.RemoteCommand
    {

        protected override void Action_ShowRawByteLength(byte[] _byte)
        {
            base.Action_ShowRawByteLength(_byte);

            Debug.LogError("receving string here");

            if (System.Text.Encoding.UTF8.GetString(_byte) == "Cheer")
            {
                if (ModelController.onTriggerAnim!=null)
                    ModelController.onTriggerAnim("excited");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Clap")
            {
                if (ModelController.onTriggerAnim != null)
                    ModelController.onTriggerAnim("clapping");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Talking")
            {
                if (ModelController.onTriggerAnim != null)
                    ModelController.onTriggerAnim("talking");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "PlayAudioOnce")
            {
                SoundController.onPlayAudioOnce(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1]);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "PlayAudioLoop")
            {
                SoundController.onPlayAudioLoop(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1]);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "NumberOfBallsToPick")
            {
                ((GameController)con).totalBallsToPick = int.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim());
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "delayAfterEachBall")
            {
                ((GameController)con).pickBallDelay = int.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim());
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "ColorSequence")
            {
                ((GameController)con).ballSetSequence.Clear();
                ((GameController)con).ballSetSequenceColors.Clear();

                Debug.LogError("here1");

                string[] colorSequence = System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim().Split(',');
                foreach (string s in colorSequence)
                {
                    switch (s)
                    {
                        case "black":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.black);
                            ((GameController)con).ballSetSequenceColors.Add(Color.black);
                            break;
                        case "blue":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.blue);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(0.02352939f, 0.1814399f, 0.5960785f, 1));
                            break;
                        case "brown":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.brown);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(0.5754717f, 0.291028f, 0, 1));
                            break;
                        case "green":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.green);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(0.200303f, 0.5943396f, 0.02523139f, 1));
                            break;
                        case "orange":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.orange);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(1, 0.2657566f, 0, 1));
                            break;
                        case "pink":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.pink);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(1, 0, 0.8989267f, 1));
                            break;
                        case "purple":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.purple);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(0.857008f, 0, 1, 1));
                            break;
                        case "red":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.red);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(0.8679245f, 0.04730861f, 0));
                            break;
                        case "white":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.white);
                            ((GameController)con).ballSetSequenceColors.Add(Color.white);
                            break;
                        case "whitered":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.whitered);
                            ((GameController)con).ballSetSequenceColors.Add(Color.white);
                            break;
                        case "yellow":
                            ((GameController)con).ballSetSequence.Add(((GameController)con).ballset.yellow);
                            ((GameController)con).ballSetSequenceColors.Add(new Color(1, 0.772643f, 0.01568627f, 1));
                            break;
                    }
                }
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "DrawBall")
            {
                Debug.LogError("drawball");
                string colorReceived = System.Text.Encoding.UTF8.GetString(_byte).Split(':')[2].Trim();
                Color tempColor=Color.black;
                Sprite tempSprite = ((GameController)con).ballset.black;
                switch (colorReceived)
                {
                    case "black":
                        tempSprite = ((GameController)con).ballset.black;
                        tempColor = Color.black;
                        break;
                    case "blue":
                        tempSprite = ((GameController)con).ballset.blue;
                        tempColor = new Color(0.02352939f, 0.1814399f, 0.5960785f, 1);
                        break;
                    case "brown":
                        tempSprite = ((GameController)con).ballset.brown;
                        tempColor = new Color(0.5754717f, 0.291028f, 0, 1);
                        break;
                    case "green":
                        tempSprite = ((GameController)con).ballset.green;
                        tempColor = new Color(0.200303f, 0.5943396f, 0.02523139f, 1);
                        break;
                    case "orange":
                        tempSprite = ((GameController)con).ballset.orange;
                        tempColor = new Color(1, 0.2657566f, 0, 1);
                        break;
                    case "pink":
                        tempSprite = ((GameController)con).ballset.pink;
                        tempColor = new Color(1, 0, 0.8989267f, 1);
                        break;
                    case "purple":
                        tempSprite = ((GameController)con).ballset.purple;
                        tempColor = new Color(0.857008f, 0, 1, 1);
                        break;
                    case "red":
                        tempSprite = ((GameController)con).ballset.red;
                        tempColor = new Color(0.8679245f, 0.04730861f, 0);
                        break;
                    case "white":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "whitered":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "yellow":
                        tempSprite = ((GameController)con).ballset.yellow;
                        tempColor = new Color(1, 0.772643f, 0.01568627f, 1);
                        break;
                }
                ((GameController)con).pickBall(int.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim()),tempColor, tempSprite);

            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "NewGame")
            {
                ((GameController)con).newGame();
            }
        }

        /// <summary>
        /// For udp client standalone
        /// </summary>
        /// <param name="str"></param>
          public void receiveString(string str)
        {
             if (str.Split(':')[0].Trim() == "DrawBall")
            {
                if (KenoDrawEffect.onDraw != null)
                    KenoDrawEffect.onDraw(true);

                string colorReceived = str.Split(':')[2].Trim();
                Color tempColor = Color.black;
                Sprite tempSprite = ((GameController)con).ballset.black;
                switch (colorReceived)
                {
                    case "black":
                        tempSprite = ((GameController)con).ballset.black;
                        tempColor = Color.black;
                        break;
                    case "blue":
                        tempSprite = ((GameController)con).ballset.blue;
                        tempColor = new Color(0.02352939f, 0.1814399f, 0.5960785f, 1);
                        break;
                    case "brown":
                        tempSprite = ((GameController)con).ballset.brown;
                        tempColor = new Color(0.5754717f, 0.291028f, 0, 1);
                        break;
                    case "green":
                        tempSprite = ((GameController)con).ballset.green;
                        tempColor = new Color(0.200303f, 0.5943396f, 0.02523139f, 1);
                        break;
                    case "orange":
                        tempSprite = ((GameController)con).ballset.orange;
                        tempColor = new Color(1, 0.2657566f, 0, 1);
                        break;
                    case "pink":
                        tempSprite = ((GameController)con).ballset.pink;
                        tempColor = new Color(1, 0, 0.8989267f, 1);
                        break;
                    case "purple":
                        tempSprite = ((GameController)con).ballset.purple;
                        tempColor = new Color(0.857008f, 0, 1, 1);
                        break;
                    case "red":
                        tempSprite = ((GameController)con).ballset.red;
                        tempColor = new Color(0.8679245f, 0.04730861f, 0);
                        break;
                    case "white":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "whitered":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "yellow":
                        tempSprite = ((GameController)con).ballset.yellow;
                        tempColor = new Color(1, 0.772643f, 0.01568627f, 1);
                        break;
                }
                ((GameController)con).pickBall(int.Parse(str.Split(':')[1].Trim()), tempColor, tempSprite);

            }
            else if (str.Split(':')[0].Trim() == "NewGame")
            {
                ((GameController)con).newGame();
            }




        }


    }

}