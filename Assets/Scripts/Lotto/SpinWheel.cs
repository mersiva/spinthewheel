﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Lotto
{
    public class SpinWheel : MonoBehaviour
    {

        private void Start()
        {
            spin();
        }

        void spin()
        {
            Sequence mySequence = DOTween.Sequence();
            mySequence.Join(transform.DORotate(new Vector3(0, 0, 360), 1, RotateMode.FastBeyond360).SetEase(Ease.Linear).OnComplete(() => {
                spin();
            }));

        }
    }

}
