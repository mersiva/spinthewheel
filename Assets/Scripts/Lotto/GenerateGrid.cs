﻿using Lotto;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lotto
{
    public class GenerateGrid : MonoBehaviour
    {
        // Start is called before the first frame update

        public Vector2 grid;
        public Vector2 offset;
        public GameObject prefab;

        public BallSet ballset;
        public GameObject winningArea;

        private void OnEnable()
        {
            GameController.onNewGame += newGame;
            GameController.onPlayVo += highlightBall;
        }

        private void OnDisable()
        {
            GameController.onNewGame -= newGame;
            GameController.onPlayVo -= highlightBall;
        }

        public void newGame()
        {
            newGame("black");
        }

        public void newGame(string col)
        {
            destroyChild();
            generateGrid(col);
            rename();
        }

        public void highlightBall(int index)
        {
            if(winningArea.transform.childCount >= 3)
            {
 
                  Destroy(winningArea.transform.GetChild(0).gameObject);
        
            }

            Debug.Log("highlight ball" + index);
            transform.GetChild(index-1).GetComponent<Image>().color = new Color(1, 1, 1, 1f);
            transform.GetChild(index - 1).GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color(1, 1, 1,1f);
        }

        [ContextMenu("generate grid")]
        public void generateGrid(string color)
        {
            Debug.Log("color is " + color);
            float startX = gameObject.transform.position.x - (offset.x * (grid.x / 2));
            float startY = gameObject.transform.position.y + (offset.y * (grid.y / 2));
            for (int y = 0; y < grid.y; y++)
            {
                for (int x = 0; x < grid.x; x++)
                {
                    GameObject temp = Instantiate(prefab, new Vector3(startX, startY, 0), Quaternion.identity);

                    switch (color)
                    {
                        case "black":
                            temp.GetComponent<Image>().sprite = ballset.black;
                            break;
                        case "blue":
                            temp.GetComponent<Image>().sprite = ballset.blue;
                            break;
                        case "brown":
                            temp.GetComponent<Image>().sprite = ballset.brown;
                            break;
                        case "green":
                            temp.GetComponent<Image>().sprite = ballset.green;
                            break;
                        case "orange":
                            temp.GetComponent<Image>().sprite = ballset.orange;
                            break;
                        case "pink":
                            temp.GetComponent<Image>().sprite = ballset.pink;
                            break;
                        case "purple":
                            temp.GetComponent<Image>().sprite = ballset.purple;
                            break;
                        case "red":
                            temp.GetComponent<Image>().sprite = ballset.red;
                            break;

                        case "yellow":
                            temp.GetComponent<Image>().sprite = ballset.yellow;
                            break;
                    }

                    temp.GetComponent<Image>().color = new Color(1, 1, 1, 0.4f);
                    temp.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color(1, 1, 1, 0.4f);
                    temp.transform.localScale = prefab.transform.localScale;
                    temp.transform.SetParent(gameObject.transform, false);
                    startX += offset.x;
                }
                startX = gameObject.transform.position.x - (offset.x * (grid.x / 2));
                startY -= offset.y;
            }

        }

        [ContextMenu("destroy grid")]
        public void destroyChild()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
            }
        }

        [ContextMenu("rename grid")]
        public void rename()
        {
            Debug.Log("renaming");
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                gameObject.transform.GetChild(i).name = "BALL" + i;
                gameObject.transform.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();

            }

        }

        public void setColor(string col)
        {

        }

        [ContextMenu("sort grid")]
        public void sort()
        {
            int index = 0;
            for (int i = gameObject.transform.childCount - 1; i >= 0; i--)
            {
                gameObject.transform.GetChild(i).SetSiblingIndex(index);
                index++;

            }

        }

    }

}
