﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lucky6PositionController : MonoBehaviour
{
    public int currentIndex;

    // Start is called before the first frame update

    private void Awake()
    {
        currentIndex = 0;

    }

    public void resetPos()
    {
        currentIndex = 0;
    }

    public Transform getCurrentPos()
    {
        return transform.GetChild(currentIndex).transform;
    }

    public void incrementIndex()
    {
        if(currentIndex < transform.childCount-1)
            currentIndex++;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
