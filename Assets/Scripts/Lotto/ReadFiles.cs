﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Lotto
{
    public class ReadFiles : MonoBehaviour
    {
        public Image backgroundImage;
        public GameController con;
        public GameObject avatar;
        string bgPath = Application.streamingAssetsPath;
        string settingsPath = Application.streamingAssetsPath + "/LottoSettings.txt";

        public string bgFilename;

        void Awake()
        {
            if(backgroundImage!=null)
            {
                if (File.Exists(bgPath + bgFilename))
                {
                    byte[] pngBytes = System.IO.File.ReadAllBytes(bgPath + bgFilename);
                    Texture2D tex = new Texture2D(2, 2);

                    tex.LoadImage(pngBytes);
                    Sprite tempSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                    backgroundImage.sprite = tempSprite;
                }
            }


            if (File.Exists(settingsPath))
            {
                string[] lines = System.IO.File.ReadAllLines(settingsPath);

                con.totalBallsToPick = int.Parse(lines[0].Split(':')[1].Trim());
                con.highestNumber = int.Parse(lines[1].Split(':')[1].Trim());

                if(int.Parse(lines[2].Split(':')[1].Trim())==1)
                {
                    con.ballset = GameObject.Find("BallSet1").gameObject.GetComponent<BallSet>();
                }
                con.pickBallDelay = int.Parse(lines[3].Split(':')[1].Trim());

                if(int.Parse(lines[4].Split(':')[1].Trim())==1)
                    con.currentGameMode = GameController.GameMode.Mode1;
                else if (int.Parse(lines[4].Split(':')[1].Trim()) == 2)
                    con.currentGameMode = GameController.GameMode.Mode2;

                string[] tempPos = lines[5].Split(':')[1].Trim().Split(',');
                con.exitPos = new Vector3(int.Parse(tempPos[0]), int.Parse(tempPos[1]), int.Parse(tempPos[2]));

                // tempPos = lines[6].Split(':')[1].Trim().Split(',');
                //con.restingPos = new Vector3(int.Parse(tempPos[0]), int.Parse(tempPos[1]), int.Parse(tempPos[2]));

                con.totalBallBeforeColorSequenceStart = int.Parse(lines[7].Split(':')[1].Trim());

                string[] colorSequence = lines[8].Split(':')[1].Trim().Split(',');
                con.ballSetSequence.Clear();
                con.ballSetSequenceColors.Clear();

                foreach (string s in colorSequence)
                {
                    switch(s)
                    {
                        case "black":
                            con.ballSetSequence.Add(con.ballset.black);
                            con.ballSetSequenceColors.Add(Color.black);
                            break;
                        case "blue":
                            con.ballSetSequence.Add(con.ballset.blue);
                            con.ballSetSequenceColors.Add(new Color(0.02352939f, 0.1814399f, 0.5960785f,1));
                            break;
                        case "brown":
                            con.ballSetSequence.Add(con.ballset.brown);
                            con.ballSetSequenceColors.Add(new Color(0.5754717f,0.291028f, 0, 1));
                            break;
                        case "green":
                            con.ballSetSequence.Add(con.ballset.green);
                            con.ballSetSequenceColors.Add(new Color(0.200303f, 0.5943396f, 0.02523139f,1));
                            break;
                        case "orange":
                            con.ballSetSequence.Add(con.ballset.orange);
                            con.ballSetSequenceColors.Add(new Color(1, 0.2657566f, 0, 1));
                            break;
                        case "pink":
                            con.ballSetSequence.Add(con.ballset.pink);
                            con.ballSetSequenceColors.Add(new Color(1, 0, 0.8989267f, 1));
                            break;
                        case "purple":
                            con.ballSetSequence.Add(con.ballset.purple);
                            con.ballSetSequenceColors.Add(new Color(0.857008f,0,1,1));
                            break;
                        case "red":
                            con.ballSetSequence.Add(con.ballset.red);
                            con.ballSetSequenceColors.Add(new Color(0.8679245f, 0.04730861f,0));
                            break;
                        case "white":
                            con.ballSetSequence.Add(con.ballset.white);
                            con.ballSetSequenceColors.Add(Color.white);
                            break;
                        case "whitered":
                            con.ballSetSequence.Add(con.ballset.whitered);
                            con.ballSetSequenceColors.Add(Color.white);
                            break;
                        case "yellow":
                            con.ballSetSequence.Add(con.ballset.yellow);
                            con.ballSetSequenceColors.Add(new Color(1, 0.772643f, 0.01568627f,1));
                            break;
                    }
                }


                if (bool.Parse(lines[9].Split(':')[1].Trim()))
                    avatar.SetActive(true);
                else
                    avatar.SetActive(false);


                //Read all sprite numbers into game controller\
                for(int i =1; i < 100; i ++)
                {
                    Sprite sp = Resources.Load<Sprite>("TransBall/"+i.ToString());
                    con.numbersSprite.Add(sp);
                }

            }


        }
    }

}