﻿using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Lotto
{
    public class VideoPlaybackUDP : Master.RemoteCommand
    {
        public MediaPlayer intro,main;
        public DisplayUGUI display;
        public Image outro;


        string[] commandRcv;



        private void OnDestroy()
        {
            
        }

        protected override void Action_ShowRawByteLength(byte[] _byte)
        {

        }
          public void receiveString(string str)
        {
            if (str.Split(':')[0].Trim() == "PlayVideo")
            {
                Debug.LogError("receving string");
                string videoPath = str.Split(':')[1].Trim();

                main.m_VideoPath = videoPath;
                main.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, videoPath, false);

                commandRcv = str.Split(':')[2].Trim().Split(',');
                playIntro();
     
            }

        }

        IEnumerator tiggerBall(int ballNo,Color tempColor,Sprite tempSprite, float delay)
        {
            yield return new WaitForSeconds(delay);
  
            ((GameController)con).pickBall(ballNo, tempColor, tempSprite);
            yield return null;

        }

        void playIntro()
        {
            Debug.LogError("playing intro");
            display._mediaPlayer = intro;
            display._mediaPlayer.Rewind(false);
            display._mediaPlayer.Play();
            outro.gameObject.SetActive(false);
        }

        void playMain()
        {
            Debug.LogError("playing main");
            display._mediaPlayer = main;
            display._mediaPlayer.Rewind(false);
            display._mediaPlayer.Play();
        }

        void showOutro()
        {
            Debug.LogError("showing outro");
            ((GameController)con).newGame();
            display._mediaPlayer = main;
            display._mediaPlayer.Stop();
            outro.gameObject.SetActive(true);
        }

        IEnumerator processBalls()
        {
            yield return new WaitForSeconds(10);
            if (GameController.onPlayVo != null)
            {
                GameController.onPlayVo(81);
                Debug.LogError("playing the winning number are");
            }
            else
            {
                Debug.LogError("onPlayVo is null ");
            }


            yield return new WaitForSeconds(1);
            float delay = 1f;
            for (int i = 0; i < commandRcv.Length; i += 2)
            {
                string colorReceived = commandRcv[i];

                Color tempColor = Color.black;
                Sprite tempSprite = ((GameController)con).ballset.black;
                switch (colorReceived)
                {
                    case "black":
                        tempSprite = ((GameController)con).ballset.black;
                        tempColor = Color.black;
                        break;
                    case "blue":
                        tempSprite = ((GameController)con).ballset.blue;
                        tempColor = new Color(0.02352939f, 0.1814399f, 0.5960785f, 1);
                        break;
                    case "brown":
                        tempSprite = ((GameController)con).ballset.brown;
                        tempColor = new Color(0.5754717f, 0.291028f, 0, 1);
                        break;
                    case "green":
                        tempSprite = ((GameController)con).ballset.green;
                        tempColor = new Color(0.200303f, 0.5943396f, 0.02523139f, 1);
                        break;
                    case "orange":
                        tempSprite = ((GameController)con).ballset.orange;
                        tempColor = new Color(1, 0.2657566f, 0, 1);
                        break;
                    case "pink":
                        tempSprite = ((GameController)con).ballset.pink;
                        tempColor = new Color(1, 0, 0.8989267f, 1);
                        break;
                    case "purple":
                        tempSprite = ((GameController)con).ballset.purple;
                        tempColor = new Color(0.857008f, 0, 1, 1);
                        break;
                    case "red":
                        tempSprite = ((GameController)con).ballset.red;
                        tempColor = new Color(0.8679245f, 0.04730861f, 0);
                        break;
                    case "white":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "whitered":
                        tempSprite = ((GameController)con).ballset.white;
                        tempColor = Color.white;
                        break;
                    case "yellow":
                        tempSprite = ((GameController)con).ballset.yellow;
                        tempColor = new Color(1, 0.772643f, 0.01568627f, 1);
                        break;
                }
                StartCoroutine(tiggerBall(int.Parse(commandRcv[i + 1]), tempColor, tempSprite, delay));
                delay += 2;
            }

            yield return null;
        }

        public void finishedIntro(MediaPlayer player, MediaPlayerEvent.EventType e, ErrorCode code)
        {
            switch (e)
            {
                case MediaPlayerEvent.EventType.FinishedPlaying:
                    playMain();
                    StartCoroutine(processBalls());
                    break;
            }

        }

        public void finishedMain(MediaPlayer player, MediaPlayerEvent.EventType e, ErrorCode code)
        {
            switch (e)
            {
                case MediaPlayerEvent.EventType.FinishedPlaying:
                    showOutro();
                    break;
            }

        }



    }

}