﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KenoDrawEffect : MonoBehaviour
{
    public delegate void OnDraw(bool status);
    public static OnDraw onDraw;

    public GameObject drawEffect;

    float currentTimer = 0;

    private void OnEnable()
    {
        onDraw += onEffect;
    }

    private void OnDisable()
    {
        onDraw -= onEffect;
    }

    void onEffect(bool status)
    {
        drawEffect.SetActive(status);
        currentTimer = 0;
    }

    public void isHitHandler(AnimationEvent animationEvent)
    {
        drawEffect.SetActive(false);

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if(currentTimer >1 && drawEffect.activeSelf)
        //{
        //    currentTimer = 0;
        //    drawEffect.SetActive(false);
        //}
        //else if(drawEffect.activeSelf)
        //{
        //    currentTimer += Time.deltaTime;
        //}


    }
}
