﻿using Lotto;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bingo
{
    public class BingoRemoteCommand : MonoBehaviour
    {
        public GenerateGrid generator;
        public GameController con;

        public void receiveString(string str)
        {
            if (str.Split(':')[0].Trim() == "BingoNewGame")
            {
                generator.newGame(str.Split(':')[1]);
                con.newGame();
              
            }
            else if (str.Split(':')[0].Trim() == "DrawBall")
            {
                Debug.Log(str + "//");
                string colorReceived = str.Split(':')[2].Trim();
                Color tempColor = Color.black;
                Sprite tempSprite = con.ballset.black;
                switch (colorReceived)
                {
                    case "black":
                        tempSprite = con.ballset.black;
                        tempColor = Color.black;
                        break;
                    case "blue":
                        tempSprite = con.ballset.blue;
                        tempColor = new Color(0.02352939f, 0.1814399f, 0.5960785f, 1);
                        break;
                    case "brown":
                        tempSprite = con.ballset.brown;
                        tempColor = new Color(0.5754717f, 0.291028f, 0, 1);
                        break;
                    case "green":
                        tempSprite = con.ballset.green;
                        tempColor = new Color(0.200303f, 0.5943396f, 0.02523139f, 1);
                        break;
                    case "orange":
                        tempSprite = con.ballset.orange;
                        tempColor = new Color(1, 0.2657566f, 0, 1);
                        break;
                    case "pink":
                        tempSprite = con.ballset.pink;
                        tempColor = new Color(1, 0, 0.8989267f, 1);
                        break;
                    case "purple":
                        tempSprite = con.ballset.purple;
                        tempColor = new Color(0.857008f, 0, 1, 1);
                        break;
                    case "red":
                        tempSprite = con.ballset.red;
                        tempColor = new Color(0.8679245f, 0.04730861f, 0);
                        break;
                    case "white":
                        tempSprite = con.ballset.white;
                        tempColor = Color.white;
                        break;
                    case "whitered":
                        tempSprite = con.ballset.white;
                        tempColor = Color.white;
                        break;
                    case "yellow":
                        tempSprite = con.ballset.yellow;
                        tempColor = new Color(1, 0.772643f, 0.01568627f, 1);
                        break;
                }
                con.pickBall(int.Parse(str.Split(':')[1].Trim()), tempColor, tempSprite);

            }

        }

    }
}

