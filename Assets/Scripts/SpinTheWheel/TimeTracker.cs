﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpinTheWheel
{
    public class TimeTracker : MonoBehaviour
    {
        // Start is called before the first frame update
        public delegate void OnMinuteChange();
        public static OnMinuteChange onMinuteChange;
        int currentMinute;

        public GameController con;
        void Start()
        {
            currentMinute = System.DateTime.Now.TimeOfDay.Minutes;
        }

        // Update is called once per frame
        void Update()
        {
            if (con.currentControl == GameController.ControlType.Time)
            {
                if (currentMinute != System.DateTime.Now.TimeOfDay.Minutes)
                {
                    currentMinute = System.DateTime.Now.TimeOfDay.Minutes;

                    if (onMinuteChange != null)
                        onMinuteChange();
                }

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (onMinuteChange != null)
                    onMinuteChange();
            }

        }

        private void OnDestroy()
        {
            onMinuteChange = null;
        }
    }

}