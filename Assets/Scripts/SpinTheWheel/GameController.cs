﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpinTheWheel
{
    public class GameController : Master.GameController
    {
        // Start is called before the first frame update
        public RectTransform LeftWheel, RightWheel;
        public Vector3 leftWheelPosition, rightWheelPosition;

        public GameMode currentMode = GameMode.TwoWheel;


        public static int completedSpinCounter;

        public delegate void OnNewGameMode();
        public static OnNewGameMode onNewGameMode;

        public int countdown;
        public int totalResultRequired;
        public bool showCountdown;

        public float delayNewGameMode = 5f;

        public bool isPlaying = false;

        public int minSpinDuration;
        public int maxSpinDuration;
        public bool zoomEnabled = false;

        public ControlType currentControl;
        public enum ControlType
        {
            Time,
            Desktop,
            Remote
        }

        public bool changeModeAutomatically = false;

        private void Awake()
        {
        
        }

        private void OnDestroy()
        {
            onNewGameMode = null;
        }

        public enum GameMode
        {
            TwoWheel,
            LeftWheel,
            RightWheel

        }

        //Two wheel
        public void changeGameMode1()
        {
            if (onNewGameMode != null)
                onNewGameMode();

            currentMode = GameMode.TwoWheel;
            LeftWheel.gameObject.SetActive(true);
            RightWheel.gameObject.SetActive(true);
            LeftWheel.anchoredPosition = new Vector2(leftWheelPosition.x, LeftWheel.anchoredPosition.y);
            RightWheel.anchoredPosition = new Vector2(rightWheelPosition.x, RightWheel.anchoredPosition.y);
        }

        //Left wheel only
        public void changeGameMode2()
        {

            if (onNewGameMode != null)
                onNewGameMode();

            currentMode = GameMode.LeftWheel;
            RightWheel.gameObject.SetActive(false);
            LeftWheel.gameObject.SetActive(true);
            LeftWheel.anchoredPosition = new Vector2(0, LeftWheel.anchoredPosition.y);
        }

        //Right wheel only
        public void changeGameMode3()
        {
            if (onNewGameMode != null)
                onNewGameMode();

            currentMode = GameMode.RightWheel;
            LeftWheel.gameObject.SetActive(false);
            RightWheel.gameObject.SetActive(true);
            RightWheel.anchoredPosition = new Vector2(0, RightWheel.anchoredPosition.y);
        }


        // Update is called once per frame
        void Update()
        {

            if (currentMode == GameMode.TwoWheel)
            {
                if (completedSpinCounter == 2)
                {
                    completedSpinCounter = 0;
                    if (ZoomCamera.onZoomTo != null && zoomEnabled)
                        ZoomCamera.onZoomTo(ZoomCamera.ZoomType.Left);

                    if (!changeModeAutomatically)
                    {
                        changeGameMode1();
                    }

                    else
                        Invoke("changeGameMode2", delayNewGameMode);

                }
            }
            else if (currentMode == GameMode.LeftWheel)
            {
                if (completedSpinCounter == 1)
                {
                    completedSpinCounter = 0;
                    if (ZoomCamera.onZoomTo != null && zoomEnabled)
                        ZoomCamera.onZoomTo(ZoomCamera.ZoomType.Middle);

                    if (!changeModeAutomatically)
                    {

                        changeGameMode2();
                    }
                    else
                        Invoke("changeGameMode3", delayNewGameMode);
                }
            }
            else if (currentMode == GameMode.RightWheel)
            {
                if (completedSpinCounter == 1)
                {
                    completedSpinCounter = 0;

                    if (ZoomCamera.onZoomTo != null && zoomEnabled)
                        ZoomCamera.onZoomTo(ZoomCamera.ZoomType.Middle);

                    if (!changeModeAutomatically)
                        changeGameMode3();
                    else
                        Invoke("changeGameMode1", delayNewGameMode);
                }
            }
        }
    }

}