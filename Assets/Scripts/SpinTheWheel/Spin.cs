﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SpinTheWheel
{
    public class Spin : MonoBehaviour
    {
        public List<string> numbers = new List<string>();

        public int noSegments;
        // Start is called before the first frame update
        RectTransform rt;
        public float currentAngle;
        float incrementAngle;
        public bool isSpinning = false;

        public bool offsetFirstRun = false;
        public GameObject fx;
        public ParticleSystem rays;
        public GameController con;

        public delegate void OnPlayVo(int soundIndex);
        public OnPlayVo onPlayVo;
        private void Awake()
        {
            rt = GetComponent<RectTransform>();
            currentAngle = rt.eulerAngles.z;


            rays.Stop();
        }

        public void initSpin()
        {

            incrementAngle = 360 / (noSegments * 1.0f);
        }

        private void OnEnable()
        {
            TimeTracker.onMinuteChange += spin;
            GameController.onNewGameMode += resetSpin;
            CountdownTimer.onCountdownCompleted += spin;
        }

        private void OnDisable()
        {
            TimeTracker.onMinuteChange -= spin;
            GameController.onNewGameMode -= resetSpin;
            CountdownTimer.onCountdownCompleted -= spin;
        }

        void resetSpin()
        {
            isSpinning = false;
            rays.Stop();
            rays.Clear();
        }

        int getIndexFromNumber(string no)
        {
            int index = 0;
            foreach(string s in numbers)
            {
                if(s==no)
                {
                    return index;
                }
                index++;
            }

            return index;

        }

        public void spin(string no)
        {
   

            if (!isSpinning)
            {
                Random.InitState(System.DateTime.Now.Millisecond);
                fx.SetActive(true);
                rays.Play();

                isSpinning = true;

                int randomSlot = getIndexFromNumber(no);

                int randomSpin = Random.Range(3, 6);
                int randomDuration = Random.Range(con.minSpinDuration, con.maxSpinDuration);
                currentAngle = (incrementAngle * randomSlot) + (360 * 3);

                if (offsetFirstRun)
                    currentAngle += (incrementAngle / 2f);

                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(transform.DORotate(new Vector3(0, 0, currentAngle), randomDuration, RotateMode.FastBeyond360).SetEase(Ease.OutCubic).OnComplete(() => {

                    fx.SetActive(false);
                    rays.Stop();
                    rays.Clear();
                    GameController.completedSpinCounter++;

                    if (onPlayVo != null)
                    {
                        if(no=="x")
                        {
                 
                            onPlayVo(numbers.Count);
                        }
                        else
                            onPlayVo(int.Parse(no));
                    }
                      
                }
                ));
            }
        }

        void spin()
        {
            if (!isSpinning)
            {
                Random.InitState(System.DateTime.Now.Millisecond);
                fx.SetActive(true);
                rays.Play();

                isSpinning = true;
                int randomSlot = Random.Range(0, noSegments);
                int randomSpin = Random.Range(3, 6);
                int randomDuration = Random.Range(con.minSpinDuration, con.maxSpinDuration);
                currentAngle = (incrementAngle * randomSlot) + (360 * 3);

                if (offsetFirstRun)
                    currentAngle += (incrementAngle / 2f);

                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(transform.DORotate(new Vector3(0, 0, currentAngle), randomDuration, RotateMode.FastBeyond360).SetEase(Ease.OutCubic).OnComplete(() => {

                    fx.SetActive(false);
                    rays.Stop();
                    rays.Clear();
                    GameController.completedSpinCounter++;


                }
                ));
            }
        }

    }

}