﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpinTheWheel
{

    public class ModelAnimHandler : MonoBehaviour
    {
        Animator m_Animator;

        private void Awake()
        {
            m_Animator = GetComponent<Animator>();
        }
        public void excited(AnimationEvent animationEvent)
        {
            m_Animator.SetBool("excited", false);
        }

        public void clapping(AnimationEvent animationEvent)
        {
            m_Animator.SetBool("clapping", false);
        }

        public void talking(AnimationEvent animationEvent)
        {
            m_Animator.SetBool("talking", false);
        }
    }

}