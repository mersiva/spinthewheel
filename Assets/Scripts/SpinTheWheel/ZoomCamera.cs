﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SpinTheWheel
{

    public class ZoomCamera : MonoBehaviour
    {
        // Start is called before the first frame update
        public Vector3 oriPos;
        public Vector3 oneWheelZoomPos;
        public Vector3 left2WheelZoomPos;
        public Vector3 right2WheelZoomPos;

        public Vector3 modelPos;

        public GameController con;

        public float oriSize;
        public float targetSize;

        public enum ZoomType
        {
            Left,
            Middle,
            Right,
            Model,
            Reset
        }

        public delegate void OnZoomTo(ZoomType zoom);
        public static OnZoomTo onZoomTo;

        private void OnEnable()
        {
            onZoomTo += goTo;
        }

        private void OnDisable()
        {
            onZoomTo -= goTo;
        }

        private void OnDestroy()
        {
            onZoomTo = null;
        }

        private void Awake()
        {

        }

        void goTo(ZoomType zoom)
        {

            Sequence mySequence = DOTween.Sequence();
            switch (zoom)
            {
                case ZoomType.Left:
                    mySequence.Join(DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, targetSize, (con.delayNewGameMode - 2 / 2)).SetEase(Ease.InOutCubic));
                    mySequence.Join(DOTween.To(() => transform.position, x => transform.position = x, left2WheelZoomPos, (con.delayNewGameMode - 2 / 2)).SetEase(Ease.InOutCubic)).OnComplete(() =>
                    {
                        Sequence mySequence1 = DOTween.Sequence();
                        mySequence1.Join(DOTween.To(() => transform.position, x => transform.position = x, right2WheelZoomPos, (con.delayNewGameMode - 2 / 2)).SetEase(Ease.InOutCubic)).OnComplete(() =>
                      resetCamBasedOnMode()
                        );
                    });

                    break;
                case ZoomType.Middle:
                    mySequence.Join(DOTween.To(() => transform.position, x => transform.position = x, oneWheelZoomPos, 1f).SetEase(Ease.InOutCubic)).OnComplete(() => resetCamBasedOnMode());
                    mySequence.Join(DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, targetSize, 1f).SetEase(Ease.InOutCubic));
                    break;
                case ZoomType.Model:
                    mySequence.Join(DOTween.To(() => transform.position, x => transform.position = x, modelPos, 1f).SetEase(Ease.InOutCubic));
                    mySequence.Join(DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, targetSize, 1f).SetEase(Ease.InOutCubic));
                    break;
                case ZoomType.Reset:
                    resetCam();
                    break;

            }

        }

        void resetCamBasedOnMode()
        {
            if (con.currentControl == GameController.ControlType.Desktop)
            {
                //minimum 1 sec
                Invoke("resetCam", con.delayNewGameMode - ((con.delayNewGameMode - 2 / 2) * 2));
            }
            else
            {
                //time base 5
                Invoke("resetCam", con.delayNewGameMode);
            }
        }

        private void resetCam()
        {
            Sequence mySequence1 = DOTween.Sequence();
            mySequence1.Join(DOTween.To(() => transform.position, x => transform.position = x, oriPos, 1f).SetEase(Ease.InOutCubic));
            mySequence1.Join(DOTween.To(() => Camera.main.orthographicSize, x => Camera.main.orthographicSize = x, oriSize, 1f).SetEase(Ease.InOutCubic));
            //transform.position = oriPos;
        }

    }

}