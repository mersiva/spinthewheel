﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SpinTheWheel
{
    public class SoundController : MonoBehaviour
    {
        // Start is called before the first frame update

        AudioSource spinFx;

        private void OnEnable()
        {
            TimeTracker.onMinuteChange += playSound;
        }

        private void OnDisable()
        {
            TimeTracker.onMinuteChange -= playSound;
        }

        void playSound()
        {
            spinFx.Play();
        }


        void Start()
        {
            spinFx = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}