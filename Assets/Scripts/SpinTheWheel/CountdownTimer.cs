﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace SpinTheWheel
{
    public class CountdownTimer : MonoBehaviour
    {


        public delegate void OnCountdownCompleted();
        public static OnCountdownCompleted onCountdownCompleted;
        // Start is called before the first frame update
        bool startCountdown = false;
        public TextMeshProUGUI text;
        float currentTimer;


        public GameController con;

        int totalSpeedNeeded;
        private void Awake()
        {

            if (con.currentControl == GameController.ControlType.Time)
            {

                GetComponent<CountdownTimer>().enabled = false;
                text.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(false);
            }

        }

        private void OnDestroy()
        {
            onCountdownCompleted = null;
        }


        private void OnEnable()
        {
            GameController.onNewGameMode += prepareNewSpin;
        }

        private void OnDisable()
        {
            GameController.onNewGameMode -= prepareNewSpin;
        }

        void setCountdown()
        {
 
            startCountdown = true;
            currentTimer = con.countdown;
            totalSpeedNeeded = con.totalResultRequired;
            text.transform.parent.gameObject.SetActive(true);
            con.isPlaying = true;
        }


        // Update is called once per frame
        void Update()
        {
            if (currentTimer > 0 && startCountdown)
            {
                currentTimer -= Time.deltaTime;

                TimeSpan time = TimeSpan.FromSeconds(Mathf.Ceil(currentTimer));
                //here backslash is must to tell that colon is
                //not the part of format, it just a character that we want in output
                string str = time.ToString(@"mm\:ss");
                text.text = str;
            }
            else if (startCountdown)
            {
                completedCountdown();

                startCountdown = false;
                text.transform.parent.gameObject.SetActive(false);

            }
        }

        void prepareNewSpin()
        {

            totalSpeedNeeded--;


            if (totalSpeedNeeded > 0)
            {
        
                Invoke("completedCountdown", con.delayNewGameMode);
            }
            else
            {

                con.isPlaying = false;
                Invoke("setCountdown", con.delayNewGameMode);
            }

        }

        void completedCountdown()
        {

            if (onCountdownCompleted != null)
                onCountdownCompleted();

        }

    }

}
