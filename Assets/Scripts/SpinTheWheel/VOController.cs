﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpinTheWheel
{
    public class VOController : MonoBehaviour
    {

        public AudioClip[] vo;
        AudioSource audioSource;
        // Start is called before the first frame update
        public Spin wheel;
        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            wheel.onPlayVo += playAudio;
        }

        private void OnDisable()
        {
            wheel.onPlayVo -= playAudio;
        }


        void playAudio(int index)
        {

              audioSource.PlayOneShot(vo[index - 1]);
        }
    }

}
