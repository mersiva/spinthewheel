﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpinTheWheel
{
    public class RemoteCommand : Master.RemoteCommand
    {
        // Start is called before the first frame update
        public Spin left, right;


        protected override void Action_ShowRawByteLength(byte[] _byte)
        {
            base.Action_ShowRawByteLength(_byte);

            if (((GameController)con).currentControl == GameController.ControlType.Desktop)
            {
  
                if (System.Text.Encoding.UTF8.GetString(_byte).Split('/')[0] == "1ResultCountdown" && !((GameController)con).isPlaying)
                {
                    ((GameController)con).countdown = int.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split('/')[1]);
                    ((GameController)con).totalResultRequired = 1;

                }
                else if (System.Text.Encoding.UTF8.GetString(_byte).Split('/')[0] == "2ResultCountdown" && !((GameController)con).isPlaying)
                {
                    ((GameController)con).countdown = int.Parse(System.Text.Encoding.UTF8.GetString(_byte).Split('/')[1]);
                    ((GameController)con).totalResultRequired = 2;
                }
            }

            if (System.Text.Encoding.UTF8.GetString(_byte) == "Mode1" && !((GameController)con).isPlaying)
                ((GameController)con).changeGameMode1();
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Mode2" && !((GameController)con).isPlaying)
                ((GameController)con).changeGameMode2();
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Mode3" && !((GameController)con).isPlaying)
                ((GameController)con).changeGameMode3();
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "ZoomToModel")
            {
                if (ZoomCamera.onZoomTo != null)
                    ZoomCamera.onZoomTo(ZoomCamera.ZoomType.Model);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "ZoomToScreen")
            {
                if (ZoomCamera.onZoomTo != null)
                    ZoomCamera.onZoomTo(ZoomCamera.ZoomType.Reset);
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Cheer")
            {
                if (ModelController.onTriggerAnim != null)
                    ModelController.onTriggerAnim("excited");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Clap")
            {
                if (ModelController.onTriggerAnim != null)
                    ModelController.onTriggerAnim("clapping");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte) == "Talking")
            {
                if (ModelController.onTriggerAnim != null)
                    ModelController.onTriggerAnim("talking");
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "Spin1" && !((GameController)con).isPlaying)
            {
                left.spin(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim());
            }
            else if (System.Text.Encoding.UTF8.GetString(_byte).Split(':')[0].Trim() == "Spin2")
            {
                right.spin(System.Text.Encoding.UTF8.GetString(_byte).Split(':')[1].Trim());
            }

        }
    }

}