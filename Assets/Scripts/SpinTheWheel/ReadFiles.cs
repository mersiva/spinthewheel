﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace SpinTheWheel
{
    public class ReadFiles : MonoBehaviour
    {
        public Image backgroundImage, leftWheelImage, rightWheelImage;
        public Spin leftWheelSpin, rightWheelSpin;

        public GameController con;
        public GameObject avatar;
        // Start is called before the first frame update
        string bgPath = Application.streamingAssetsPath;
        string leftWheelPath = Application.streamingAssetsPath + "/left wheel.png";
        string rightWheelPath = Application.streamingAssetsPath + "/right wheel.png";
        string settingsPath = Application.streamingAssetsPath + "/SpinTheWheelSettings.txt";

        public string bgFilename;
        void Awake()
        {
            if (File.Exists(bgPath + bgFilename))
            {
                byte[] pngBytes = System.IO.File.ReadAllBytes(bgPath + bgFilename);

                Texture2D tex = new Texture2D(2, 2);

                tex.LoadImage(pngBytes);
                Sprite tempSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                backgroundImage.sprite = tempSprite;
            }

            if (File.Exists(leftWheelPath))
            {
                byte[] pngBytes = System.IO.File.ReadAllBytes(leftWheelPath);
                Texture2D tex = new Texture2D(2, 2);

                tex.LoadImage(pngBytes);
                Sprite tempSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                leftWheelImage.sprite = tempSprite;
            }

            if (File.Exists(rightWheelPath))
            {
                byte[] pngBytes = System.IO.File.ReadAllBytes(rightWheelPath);
                Texture2D tex = new Texture2D(2, 2);

                tex.LoadImage(pngBytes);
                Sprite tempSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                rightWheelImage.sprite = tempSprite;
            }

            if (File.Exists(settingsPath))
            {
                string[] lines = System.IO.File.ReadAllLines(settingsPath);


                leftWheelSpin.noSegments = int.Parse(lines[0].Split(':')[1].Trim());
                leftWheelSpin.initSpin();
                rightWheelSpin.noSegments = int.Parse(lines[1].Split(':')[1].Trim());
                rightWheelSpin.initSpin();

                if (int.Parse(lines[2].Split(':')[1].Trim()) == 1)
                    con.changeGameMode1();
                else if (int.Parse(lines[2].Split(':')[1].Trim()) == 2)
                    con.changeGameMode2();
                else if (int.Parse(lines[2].Split(':')[1].Trim()) == 3)
                    con.changeGameMode3();

                if (bool.Parse(lines[3].Split(':')[1].Trim()))
                    con.changeModeAutomatically = true;
                else
                    con.changeModeAutomatically = false;

                if (lines[4].Split(':')[1].Trim() == "desktop")
                {
                    con.currentControl = GameController.ControlType.Desktop;
                }
                else if (lines[4].Split(':')[1].Trim() == "time")
                    con.currentControl = GameController.ControlType.Time;
                else if (lines[4].Split(':')[1].Trim() == "remote")
                    con.currentControl = GameController.ControlType.Remote;


                con.showCountdown = (bool.Parse(lines[5].Split(':')[1].Trim()));
                con.countdown = (int.Parse(lines[6].Split(':')[1].Trim()));
                con.totalResultRequired = (int.Parse(lines[7].Split(':')[1].Trim()));
                con.delayNewGameMode = (int.Parse(lines[8].Split(':')[1].Trim()));
                con.minSpinDuration = (int.Parse(lines[9].Split(':')[1].Trim()));
                con.maxSpinDuration = (int.Parse(lines[10].Split(':')[1].Trim()));
                con.zoomEnabled = (bool.Parse(lines[11].Split(':')[1].Trim()));

                if (bool.Parse(lines[12].Split(':')[1].Trim()))
                    avatar.SetActive(true);
                else
                    avatar.SetActive(false);

            }






        }

    }

}