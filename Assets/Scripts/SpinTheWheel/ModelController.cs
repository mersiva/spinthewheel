﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpinTheWheel
{
    public class ModelController : MonoBehaviour
    {
        // Start is called before the first frame update
        Animator animator;

        public delegate void OnTriggerAnim(string str);
        public static OnTriggerAnim onTriggerAnim;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void OnDestroy()
        {
            onTriggerAnim = null;
        }

        private void OnEnable()
        {
            TimeTracker.onMinuteChange += spinStarted;
            GameController.onNewGameMode += spinStop;
            onTriggerAnim += triggerAnim;
            CountdownTimer.onCountdownCompleted += spinStarted;
        }

        private void OnDisable()
        {
            TimeTracker.onMinuteChange -= spinStarted;
            GameController.onNewGameMode -= spinStop;
            onTriggerAnim -= triggerAnim;
            CountdownTimer.onCountdownCompleted -= spinStarted;
        }

        void triggerAnim(string str)
        {
            animator.SetBool(str, true);
        }

        void spinStarted()
        {
            animator.SetBool("spinningStart", true);
        }

        void spinStop()
        {
            animator.SetBool("spinningStart", false);
        }
    }

}