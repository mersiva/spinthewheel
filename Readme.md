# Live Game Engine Server

Contains two game mode Spin the wheel and Lotto  

Unity Version 2019.3.7f1  


## StartupSettings.txt

Start game with keno

```bash
GameMode:keno
```

Start game with lotto

```bash
GameMode:lotto
```

Start game with spin the wheel

```bash
GameMode:spinthewheel
```


## Notes

Set startup settings in /StreamingAssets/StartupSettings.txt  
Default background is background.png in /StreamingAssets/  

